import Adafruit_CharLCD as LCD
import requests

from lxml import etree

from pi_app import Node
from pi_app import App
from pi_app import AdafruitCharLCDDisplay
from pi_app import ConsoleInput

def london_tube_line(app, node, *argvs):
	r = requests.get('http://cloud.tfl.gov.uk/TrackerNet/PredictionSummary/' + argvs[0], headers = {'Accept-Charset': 'utf-8'})
	r.encoding = 'utf-8'

	xml = etree.fromstring(r.content)

	stations = xml.xpath('/ROOT/S')
	node.nodes = []

	for station in stations:

		station_node = Node(station.get('N'))
		platforms = station.xpath('P')

		for platform in platforms:
			platform_node = Node(platform.get('N'))

			arrivals = platform.xpath('T')

			for arrival in arrivals:
				arrival_node = Node('%s %s' % (arrival.get('C'), arrival.get('DE')))

				platform_node.add_node(arrival_node)

			station_node.add_node(platform_node)

		node.add_node(station_node)

	return True

adafruit_driver = LCD.Adafruit_CharLCDPlate()
adafruit_driver.set_color(0.0, 0.0, 1.0)
adafruit_driver.set_backlight(True)

nodes = []

nodes.append(Node('Bakerloo', None, london_tube_line, 'B'))
nodes.append(Node('Central', None, london_tube_line, 'C'))
nodes.append(Node('District', None, london_tube_line, 'D'))
nodes.append(Node('Circle', None, london_tube_line, 'H'))
nodes.append(Node('Jubilee', None, london_tube_line, 'J'))
nodes.append(Node('Metropolitan', None, london_tube_line, 'M'))
nodes.append(Node('Northern', None, london_tube_line, 'N'))
nodes.append(Node('Picadilly', None, london_tube_line, 'P'))
nodes.append(Node('Victoria', None, london_tube_line, 'V'))
nodes.append(Node('Waterloo', None, london_tube_line, 'W'))

display_adapter = AdafruitCharLCDDisplay(adafruit_driver)
input_adapter = ConsoleInput()

app = App(input_adapter, nodes, display_adapter)
app.run()
