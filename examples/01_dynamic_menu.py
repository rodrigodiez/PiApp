import Adafruit_CharLCD as LCD
import requests

from pi_app import Node
from pi_app import App
from pi_app import AdafruitCharLCDDisplay
from pi_app import ConsoleInput

def google_menu_build(app, node, *argvs):
	r = requests.get('https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=' + argvs[0])
	data = r.json()

	node.nodes = []

	for result in data['responseData']['results']:
		node.add_node(Node(result['titleNoFormatting']))

	return True

adafruit_driver = LCD.Adafruit_CharLCDPlate()
adafruit_driver.set_color(0.0, 0.0, 1.0)
adafruit_driver.set_backlight(True)

nodes = []

nodes.append(Node('Raspberry images', None, google_menu_build, 'raspberry'))
nodes.append(Node('Python images', None, google_menu_build, 'python'))
nodes.append(Node('London images', None, google_menu_build, 'london'))

display_adapter = AdafruitCharLCDDisplay(adafruit_driver)
input_adapter = ConsoleInput()

app = App(input_adapter, nodes, display_adapter)

app.run()
