import Adafruit_CharLCD as LCD

from pi_app import Node
from pi_app import App
from pi_app import AdafruitCharLCDDisplay
from pi_app import ConsoleInput

adafruit_driver = LCD.Adafruit_CharLCDPlate()
adafruit_driver.set_color(1.0, 1.0, 1.0)
adafruit_driver.set_backlight(True)

nodes = []

for x in range(1,11):
	node = Node('Node %d' % (x))

	for y in range(1,11):
		sub_node = Node('Node %d-%d' % (x, y))

		for z in range(1,11):
			sub_sub_node = Node('Node %d-%d-%d' % (x,y,z))
			sub_node.add_node(sub_sub_node)

		node.add_node(sub_node)

	nodes.append(node)

display_adapter = AdafruitCharLCDDisplay(driver=adafruit_driver)
input_adapter = ConsoleInput()

app = App(input_adapter, nodes, display_adapter)

app.run()